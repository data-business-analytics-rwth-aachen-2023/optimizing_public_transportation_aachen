Parameter
   P_Sample(s) 'Possible # new stops'
    handle(s)     'store the instance handle'
    handleG(s)     'store the instance handle'
*KPI
    newStation(s,j)
    currentStation(j)
    totalnewStation(s)
    totalcurrentStation
    totalPeople

    currentcoverage(i,j) binary matrix. 1 if i distance from i to j is lower or equal to l. 0 otherwise
    currentService(i,j)
    totalnewStops
    currentServedPeople
    newServedPeople(s)
    minDist(i)
    
    coverage(i,j) binary matrix. 1 if i distance from i to j is lower or equal to l. 0 otherwise
    currentAverageTravelDistance
    currentcumulativeTravelDistance
    newcumulativeTravelDistance(s)
    newAverageTravelDistance(s)
    totalConnections
;



loop((j,s)$(candidate(j) and ord(s)=ord(j)),
P_Sample(s)=ord(j);
);


loop((j,k),
if(ord(j)<>ord(k) and (ord(k)=ord(j)+1 or ord(k)=ord(j)-1),
e(j,k)=yes
);
);


latCand(j) = latCand(j)*pi/180;
longCand(j) = longCand(j)*pi/180;
Latxpoly(i) = Latxpoly(i)*pi/180;
Longypoly(i) = Longypoly(i)*pi/180;

aa(i,j)=sqr(sin((Latxpoly(i)-latCand(j))/2)) +
                        cos(latCand(j)) * cos(Latxpoly(i)) * sqr(sin((Longypoly(i)-longCand(j))/2));

distance(i,j) = earthRadius * 2 * arctan2(sqrt(aa(i,j)),sqrt(1 - aa(i,j)));
R=smax((i,j)$(not current(j)),distance(i,j));
* closest(i,j)= if sum(j,coverage(j))>1 then smin(dist);

minDist(i)= smin((j)$(distance(i,j)>0),distance(i,j));
currentcoverage(i,j)$(current(j) and distance(i,j)<=minDist(i))=1;
coverage(i,j)$(distance(i,j)<=R)=1;


***************** Model 2: determine optimal solution  - new stops  **********************

free Variable
G  cumulative travel distance
Binary variable
xx(j) Binary variable. 1 if we locate the candidate public transport stop j 0 otherwise
y(i,j) 1 if passenger demand point i is covered by the candidate public transport stop j 0 otherwise;

display distance,coverage;



Equation
objectiveFunction min cumulative travel distance
cover each grid i must be served by a station j
coupling If gird i is served it is because a station j is open
p_constraint The maximum number of stations to open is p ;


objectiveFunction..G=e=sum((i,j),c(i)*distance(i,j)*y(i,j));
cover(i)..sum(j,coverage(i,j)*y(i,j))=g=1;
coupling(i,j)..xx(j)=l=y(i,j);
p_constraint..sum(j$(not current(j)),xx(j))=e=p;

Model district1 /all/;
*Solve district1 MIN G using MIP;

district1.solveLink = %solveLink.AsyncGrid%;  
district1.limCol    = 0;
district1.limRow    = 0;
district1.solPrint  = %solPrint.Quiet%;

loop(s,
    p=P_Sample(s);
   Solve district1 MIN G using MIP;
*save instance handle
   handle(s) = district1.handle;    
);


Parameter
    rep_Y(s,i,j)
    rep_XX(s,j)
    rep_G(s)
    repy        'summary report';

* we use the handle parameter to indicate that the solution has been collected
repeat
   loop(s$handlecollect(handle(s)),
        rep_Y(s,i,j)=y.l(i,j);
        rep_XX(s,j)=xx.l(j);
        rep_G(s)=G.l;
        repy(s,'modelstat') =  district1.modelStat ;
        repy(s,'solvestat') = district1.solveStat ;
        repy(s,'resusd'   ) = district1.resUsd;
        repy(s,'objval')    = district1.objVal;
      display$handledelete(handle(s)) 'trouble deleting handles';
*indicate that we have loaded the solution
      handle(s) = 0;    
   );

*// wait until all models are loaded
until card(handle) = 0 or timeelapsed > 200;  



*** output parameters
Parameter
currentminDist(i)
currentmaxDist(i)
maxmincurrentDist
mincurrentDist
maxcurrentDist        

    newminDist(s,i)
    newmaxDist(s,i)   
    minnewDist(s)
    maxminnewDist(s)
    maxnewDist(s)
***
    isServed(s,i)
    isServedcurrent(i)
;
********* calculating the physical effort that passengers currently have to make


*******

currentStation(j)$(current(j))=1;
newStation(s,j)$(not current(j) and rep_XX(s,j)>0)=1;
if(card(current)>0,
currentminDist(i)= smin((j)$(distance(i,j)>0 and current(j)),distance(i,j));
currentmaxDist(i)= smax((j)$(distance(i,j)>0 and current(j)),distance(i,j));
else
currentminDist(i)= 0;
currentmaxDist(i)= 0;
);


* Merging current stops and new stops
newminDist(s,i)=smin((j)$( distance(i,j)>0 and ( current(j) or rep_XX(s,j)>0)),distance(i,j));
newmaxDist(s,i) =smax((j)$( distance(i,j)>0 and ( current(j) or rep_XX(s,j)>0)),distance(i,j));

mincurrentDist=smin(i,currentminDist(i));
maxmincurrentDist=smax(i,currentminDist(i));
maxcurrentDist=smax(i,currentmaxDist(i));

minnewDist(s)=smin(i,newminDist(s,i));
maxminnewDist(s)=smax(i,newminDist(s,i));
maxnewDist(s)=smax(i,newmaxDist(s,i));

totalnewStation(s)=sum(j,newStation(s,j));
totalcurrentStation=sum(j,currentStation(j));

currentService(i,j)$(current(j) and currentcoverage(i,j)>0)=1;
totalConnections=sum((i,j)$(coverage(i,j)>0),1);

totalPeople=sum((i),c(i));

isServedcurrent(i)$(sum(j,currentService(i,j))>0)=1;
currentServedPeople=min(totalPeople,sum(i,c(i)*isServedcurrent(i))) ;



isServed(s,i)$(sum(j,rep_Y(s,i,j))>0)=1;
newServedPeople(s)=sum(i,c(i)*isServed(s,i));



currentAverageTravelDistance=sum((i),currentminDist(i))/card(i);
currentcumulativeTravelDistance=sum((i),c(i)*currentminDist(i));

newAverageTravelDistance(s)=sum((i),newminDist(s,i))/card(i);
newcumulativeTravelDistance(s)=sum((i),c(i)*newminDist(s,i));
 


$exit