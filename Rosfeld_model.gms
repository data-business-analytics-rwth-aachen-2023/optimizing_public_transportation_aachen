Sets
j set of potential bus stop locataion /1*39/
current(j)
candidate(j)  /1*27/
s set of scenarios  /1*27/
i set of demand points /1*5/
e(j,j) set of candidate stations on the same path
;

alias(j,k);

current(j)$(ord(j)>27)=yes;
Parameters
longCand(j)    longitude angle candidate stop    
/
1   6.056353939
2   6.057075903
3   6.057920961
4   6.058992387
5   6.059605394
6   6.0619659
7   6.0620591
8   6.06223
9   6.0623079
10  6.0626851
11  6.0637546
12  6.0626851
13  6.0630642
14  6.0641806
15  6.0644997
16  6.064908745
17  6.0637546
18  6.0635986
19  6.063192
20  6.0630844
21  6.0627036
22  6.0624482
23  6.0624126
24  6.0624255
25  6.0626851
26  6.064478832
27  6.0637546
28  6.052724
29  6.052724
30  6.05984
31  6.05984
32  6.055533
33  6.055533
34  6.052509
35  6.052509
36  6.055273
37  6.055273
38  6.056308135
39  6.06222493
/

latCand(j)     latitude angle candidate stop  
/
1   50.76512763
2   50.76563846
3   50.76600704
4   50.76622073
5   50.76623738
6   50.7690447
7   50.7689924
8   50.7688384
9   50.7687866
10  50.7684818
11  50.7673908
12  50.7684818
13  50.7686682
14  50.769187
15  50.769351
16  50.76960706
17  50.7673908
18  50.7674816
19  50.7677214
20  50.7677852
21  50.7680465
22  50.7682308
23  50.7682844
24  50.7683339
25  50.7684818
26  50.76695732
27  50.7673908
28  50.765841
29  50.765841
30  50.766152
31  50.766152
32  50.767656
33  50.767656
34  50.763414
35  50.763414
36  50.763907
37  50.763907
38  50.76885272
39  50.76979793
/
Longypoly(i) longitud angle of grid i
/
1   6.056837878
2   6.057007062
3   6.060601761
4   6.064196683
5   6.064365446
/
Latxpoly(i) latitude angle of grid i
/
1   50.76830675
2   50.76561246
3   50.76705247
4   50.76849237
5   50.76579805
/
c(i) # of inhabitans in grid i
/
1   310.9563637
2   345.5176144
3   388.0539379
4   355.3746185
5   172.0764999
/
Xcand(j) x coordinate of candidate and current stations j
Ycand(j)  y coordinate of candidate stations j
xpoly(i) x coordinate of grid i
ypoly(i) y coordinate of grid i

distance(i,j) euclidian distances from i to j in km
p total of locations to open
aa(i,j)
earthRadius 'radius of earth (miles)' / 6371 /
R Largest distance between a station and a residential area
;

$include general_model.gms

execute_unload  "distanceProblem_Rosfeld.gdx"