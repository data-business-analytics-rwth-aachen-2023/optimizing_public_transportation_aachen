parameter
pctime
start;

start=jnow;

$onCheckErrorLevel
$call  gams   Beeckstr_model.gms
$call  gams   Blücherplatz_model.gms
$call  gams   "Eckenberger Str._model.gms"
$call  gams   Friedrichstr_model.gms
$call  gams   "Hanbrucher Str_model.gms"
$call  gams   "Kamper Str_model.gms"
$call  gams   Kongressstr_model.gms
$call  gams   "Mittlere Bismarkstr_model.gms"
$call  gams   "Mittleres Tittardsfeld_model.gms"
$call  gams   Neustr_model.gms
$call  gams   Nizzaalle_model.gms
$call  gams   Oranienstr_model.gms
$call  gams   Rosfeld_model.gms
$call  gams   Scheibenstr_model.gms
$call  gams   Schildstr_model.gms
$call  gams   Stettiner_Str_model.gms
$call  gams   Steppenberg_model.gms


pctime=  (jnow - start)*24*3600;
display pctime,start;